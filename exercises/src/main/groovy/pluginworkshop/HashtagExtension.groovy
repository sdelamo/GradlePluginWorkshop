// tag::extension-skeleton[]
package pluginworkshop

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.util.CollectionUtils

@CompileStatic
class HashtagExtension {

    HashtagExtension(Project project) { // <1>
        this.project = project
    }

    private Project project

    // end::extension-skeleton[]
    // tag::tags-methods[]
    private List<Object> tags = []

    // tag::tags-methods-detail[]
    void setTags(Iterable<Object> replaceWith) {
        // end::tags-methods[]
        this.tags.clear() // <1>
        this.tags.addAll(replaceWith)
        // tag::tags-methods[]
    }

    void tags(Object... newTags) {
        // end::tags-methods[]
        this.tags.addAll(newTags)
        // tag::tags-methods[]
    }

    // end::tags-methods-detail[]

    // tag::evaluating-closures[]
    Set<String> getTags() {
        // end::tags-methods[]
        Set<String> converted = []
        for( Object tag in tags) {
            switch(tag) {
                case Closure:
                    CollectionUtils.stringize([((Closure)tag).call()],converted) // <1>
                    break
                default:
                    CollectionUtils.stringize([tag],converted) // <2>
            }
        }
        converted
        // tag::tags-methods[]
    }
    // end::evaluating-closures[]
    // end::tags-methods[]

    // tag::twittertags[]
    Set<String> getTwittertags() {
        getTags().collect { "#${it}".toString() } as Set // <1>
    }
    // end::twittertags[]

    // tag::extension-skeleton[]
}
// end::extension-skeleton[]
