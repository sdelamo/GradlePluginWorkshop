// tag::test-setup[]
package pluginworkshop

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

class HashtagExtensionSpec extends Specification {

    Project project = ProjectBuilder.builder().build()

    def 'Configuring a hashtag extension should follow Gradle conventions'() {

        given: 'A hashtag extension object'
        HashtagExtension ext = project.extensions.create(
            'hashtags', HashtagExtension, project) // <1>

        when: 'Tags are configured in various ways'
        project.allprojects {
            hashtags {
                tags 'grails' // <2>
                tags "${'gr' + 'oovy'}", 'gradle' // <3>
                tags {'greach'} // <4>
            }
        }

        then: 'All tags should be available a unique set of strings'
        ext.tags.contains 'gradle'
        ext.tags.contains 'groovy'
        ext.tags.contains 'grails'
        ext.tags.contains 'greach'

        ext.twittertags.contains '#greach'
    }
}
// end::test-setup[]