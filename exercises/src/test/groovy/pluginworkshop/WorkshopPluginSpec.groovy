// tag::first-test[]
package pluginworkshop

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

class WorkshopPluginSpec extends Specification {

    Project project = ProjectBuilder.builder().build() // <1>

    def 'Plugin must load when applied'() {

        when: 'I apply the plugin by identifier'
        project.allprojects { // <2>
            apply plugin : 'com.example.pluginworkshop' // <3>
        }

        then: 'It must load without a failure'
        project.ext.workshop == '¡Hola, Greach 2017!'
    }
    // end::first-test[]

    // tag::file-manifest[]
    def 'A fileManifest task must be added'() {

        when: 'The plugin is applied'
        project.allprojects {
            apply plugin : 'com.example.pluginworkshop'
        }

        then: "A task called 'fileManifest' is added"
        project.tasks.getByPath('fileManifest') instanceof FileManifest // <1>
    }
    // end::file-manifest[]

    // tag::hashtags-extension[]
    def 'A hashtags extension must be added'() {

        when: 'The plugin is applied'
        project.allprojects {
            apply plugin : 'com.example.pluginworkshop'
        }

        then: "An extension called 'hashtags' is added"
        project.extensions.getByName('hashtags') instanceof HashtagExtension // <1>
    }
    // end::hashtags-extension[]

// tag::first-test[]
}
// end::first-test[]