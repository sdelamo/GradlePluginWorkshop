= Writing Gradle Plugins in Groovy : Resources
Schalk W. Cronjé <ysb33r@gmail.com>

* https://docs.gradle.org/current/userguide/javaGradle_plugin.html[Java Gradle Plugin Development plugin] is a helper plugin which also makes configuration of `TestKit` slightly easier. Personal experience is that one soon run into limitations with this plugin.
* https://github.com/ajoberstar/gradle-stutter[Stutter] is another library by https://github.com/ajoberstar[Andy Oberstar] to aid with plugin development. It focuses on the cases where you might have to run the same tests against all supported versions, but don't want to deal with making each test loop over those versions (or `@Unroll` with a `where` block in Spock). `Stutter` generates one task per supported version for you and runs the full set of tests against it.
* Publish your plugin to https://plugins.gradle.org/docs/submit[Gradle Plugin portal].
* https://ysb33r.github.io/grolifant/[Grolifant] - A library to support plugin development, especially in Groovy.
* https://leanpub.com/idiomaticgradle[Idiomatic Gradle] - 25 recipes for plugin authors and https://leanpub.com/idiomaticgradlepluginsvol2[Idiomatic Gradle Vol 2] which is currently being written.


